package wda

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

type Dur struct {
	Date     time.Time
	Duration time.Duration
}

type Pass struct {
	TimeStamp time.Time
	Direction bool
}

type PassStamp map[string][]Pass
type PassDuration map[string]time.Duration

func parsePasses(fp string) PassStamp {
	parseTimeForm := "02.01.2006-15:04:05"
	f, err := os.Open(fp)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()
	r := csv.NewReader(bufio.NewReader(f))
	result, _ := r.ReadAll()
	m := make(PassStamp)
	for a := range result {

		d := false
		t := result[a][0] + "-" + result[a][1]
		tp, err := time.Parse(parseTimeForm, t)
		if err != nil {
			log.Fatal(err)
		}
		if string(result[a][3][len(result[a][3])-1:]) == "S" {
			d = true
		} else if string(result[a][3][len(result[a][3])-1:]) == "U" {
			d = false
		} else {
			if string(result[a][4]) == "/2" {
				d = false
			} else {
				d = true
			}
		}
		n := strings.TrimSpace(result[a][8])
		p := Pass{
			TimeStamp: tp,
			Direction: d,
		}
		m[n] = append(m[n], p)
	}
	return m
}
func (a PassStamp) calcTime() PassDuration {
	var td time.Duration
	m := make(PassDuration)
	for b := range a {
		td = 0

		for i := len(a[b]) - 1; i >= 0; i-- {
			if !a[b][i].Direction {
				for y := i; y >= 0; y-- {
					if a[b][y].Direction {
						td += a[b][i].TimeStamp.Sub(a[b][y].TimeStamp)
						i -= (i - y)
						break
					}
				}
			}
		}
		m[b] = td
	}
	return m
}

func (p PassDuration) toCSV(filepath string) {
	file, err := os.Create(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()
	var s []string
	var data [][]string

	for a := range p {
		s = nil
		s = append(s, a)
		s = append(s, fmt.Sprintf("%.2f", p[a].Hours()))

		data = append(data, s)
	}
	writer.WriteAll(data)
}
