package finnishbankholidays

import (
	"math"
	"time"
)

type HolidayName string
type Holidays map[HolidayName]time.Time
type holiday struct {
	name HolidayName
	date time.Time
}
type data struct {
	name  string
	found bool
}

const (
	Easter                  HolidayName = "Pääsiäinen"
	AllSaintsDay            HolidayName = "Pyhäinpäivä"
	AscensionDay            HolidayName = "Helatorstai"
	GoodFriday              HolidayName = "Pitkäpejantai"
	EasterMonday            HolidayName = "Toinen pääsiäispäivä"
	Pentecost               HolidayName = "Helluntai"
	Midsummer               HolidayName = "Juhannuspäivä"
	NewYearsDay             HolidayName = "Uudenvuodenpäivä"
	Epiphany                HolidayName = "Loppiainen"
	InternationalWorkersDay HolidayName = "Vappu"
	IndependenceDay         HolidayName = "Itsenäisyyspäivä"
	ChristmasDay            HolidayName = "Joulupäivä"
	SaintStephensDay        HolidayName = "Tapaninpäivä"
)

func easterCalc(t time.Time) holiday {
	year := float64(t.Year())
	g := math.Mod(year, 19)
	c := math.Floor(year / 100)
	h := (c - math.Floor(c/4) - math.Floor((8*c+13)/25) + 19*g + 15)
	h = math.Mod(h, 30)
	i := h - math.Floor(h/28)*(1-math.Floor(29/(h+1))*math.Floor((21-g)/11))
	j := (year + math.Floor(year/4) + i + 2 - c + math.Floor(c/4))
	j = math.Mod(j, 7)
	l := i - j
	month := 3 + math.Floor((l+40)/44)
	day := l + 28 - 31*math.Floor(month/4)
	date := time.Date(t.Year(), time.Month(month), int(day), 0, 0, 0, 0, t.Location())
	s := holiday{name: Easter, date: date}
	return s
}

func easterMondayCalc(t time.Time) holiday {
	easterMondayDate := t.AddDate(0, 0, 1)
	s := holiday{name: EasterMonday, date: easterMondayDate}
	return s
}

func goodFridayCalc(t time.Time) holiday {
	goodFridayDate := t
	for {
		goodFridayDate = goodFridayDate.AddDate(0, 0, -1)
		if goodFridayDate.Weekday().String() == "Friday" {
			break
		}
	}
	s := holiday{name: GoodFriday, date: goodFridayDate}
	return s
}

func pentecostCalc(t time.Time) holiday {
	pentecostDate := t.AddDate(0, 0, 7*7)
	s := holiday{name: Pentecost, date: pentecostDate}
	return s
}

func midsummerCalc(t time.Time) holiday {
	midsummerDate := time.Date(t.Year(), time.June, 19, 0, 0, 0, 0, t.Location())
	for {
		if midsummerDate.Weekday().String() != "Saturday" {
			midsummerDate = midsummerDate.AddDate(0, 0, 1)
		} else {
			break
		}
	}
	s := holiday{name: Midsummer, date: midsummerDate}
	return s
}

func ascensionDayCalc(t time.Time) holiday {
	ascensionDayDate := t.AddDate(0, 0, 39)
	s := holiday{name: AscensionDay, date: ascensionDayDate}
	return s
}

func allSaintsDayCalc(t time.Time) holiday {
	allSaintsDayDate := time.Date(t.Year(), time.October, 31, 0, 0, 0, 0, t.Location())
	for {
		if allSaintsDayDate.Weekday().String() == "Saturday" {
			break
		}
		allSaintsDayDate = allSaintsDayDate.AddDate(0, 0, 1)
	}
	s := holiday{name: AllSaintsDay, date: allSaintsDayDate}
	return s
}

func calc(t time.Time) Holidays {
	holidays := make(map[HolidayName]time.Time)
	easterCalc(t).addHoliday(holidays)
	allSaintsDayCalc(holidays["Pääsiäinen"]).addHoliday(holidays)
	ascensionDayCalc(holidays["Pääsiäinen"]).addHoliday(holidays)
	goodFridayCalc(holidays["Pääsiäinen"]).addHoliday(holidays)
	easterMondayCalc(holidays["Pääsiäinen"]).addHoliday(holidays)
	pentecostCalc(holidays["Pääsiäinen"]).addHoliday(holidays)
	midsummerCalc(t).addHoliday(holidays)

	makeHoliday(NewYearsDay, t.Year(), time.January, 1, t.Location()).addHoliday(holidays)
	makeHoliday(Epiphany, t.Year(), time.January, 6, t.Location()).addHoliday(holidays)
	makeHoliday(InternationalWorkersDay, t.Year(), time.May, 1, t.Location()).addHoliday(holidays)
	makeHoliday(IndependenceDay, t.Year(), time.December, 6, t.Location()).addHoliday(holidays)
	makeHoliday(ChristmasDay, t.Year(), time.December, 25, t.Location()).addHoliday(holidays)
	makeHoliday(SaintStephensDay, t.Year(), time.December, 26, t.Location()).addHoliday(holidays)
	return holidays
}

func (h Holidays) compare(t time.Time) (string, bool) {
	for name, date := range h {
		if t.Equal(date) {
			return string(name), true
		}
	}
	return "", false
}

func makeHoliday(s HolidayName, year int, month time.Month, day int, loc *time.Location) holiday {
	a := holiday{name: s, date: time.Date(year, month, day, 0, 0, 0, 0, loc)}
	return a
}

func (h holiday) addHoliday(ho map[HolidayName]time.Time) {
	ho[h.name] = h.date
	return
}

func init() {
}

func HolidaysOf(year int) map[HolidayName]time.Time {
	holidays := calc(time.Date(year, 1, 1, 0, 0, 0, 0, time.Now().Location()))
	return holidays
}

func (name HolidayName) Of(year int) time.Time {
	holidays := calc(time.Date(year, 1, 1, 0, 0, 0, 0, time.Now().Location()))
	for hname, date := range holidays {
		if name == hname {
			return date
		}
	}
	return time.Time{}
}

func IsHoliday(t time.Time) (string, bool) {
	channel := make(chan data)
	go func() {
		name, found := calc(t).compare(t)
		data := data{name: name, found: found}
		channel <- data
	}()
	received := <-channel
	return received.name, received.found
}
