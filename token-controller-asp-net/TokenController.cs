using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Tiketti.Models;
using Tiketti;
using System.Security.Cryptography;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Tiketti.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IConfiguration _config;

        public TokenController(IConfiguration config)
        {
            _config = config;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken([FromBody]LoginModel login)
        {
            IActionResult response = Unauthorized();
            var user = Authenticate(login);

            if (user != null)
            {
                var tokenString = BuildToken(user);
                response = Ok(new { token = tokenString, loggedin = user, perm = user.PermissionsId.ToString()});
                CookieOptions option = new CookieOptions();
                option.Expires = DateTime.Now.AddMinutes(10);
                Response.Cookies.Append("token",tokenString,option);
            }

            return response;
        }

        [AllowAnonymous]
        [HttpPost("refresh")]
        public IActionResult HandleRefresh([FromBody] RefreshTokenModel body)
        {
            try
            {
                var cp = GetPrincipalFromExpiredToken(body.Jwt);

                if (cp != null)
                {
                    var email = cp.Claims.First(c => c.Type == "email");
                    ProfilesDataAccessLayer profileAccess = new ProfilesDataAccessLayer();
                    var user = profileAccess.GetProfileByEmail(email.Value);
                    //Tässä välissä katsottaisiin onko validi refresh token
                    if (body.RefreshToken == user.RefreshToken)
                    {
                        var jwt = BuildToken(user);
                        var refreshToken = GenerateToken();


                        return Ok(new { token = jwt, refresh = refreshToken });
                    }
                    else
                    {
                        //return fuk u;
                        return Unauthorized();
                    }
                }
                else
                {
                    //return fuk u
                    return Unauthorized();
                }
            } catch (Exception e)
            {
                return Unauthorized();
            }

        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"])),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }
        /*
         * Build the jwt
         */
        private string BuildToken(Profiles user)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(CustomClaimTypes.Permissions, user.PermissionsId.ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, user.Name),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(10),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private Profiles Authenticate(LoginModel login)
        {
            Profiles user = null;

            ProfilesDataAccessLayer profileAccess = new ProfilesDataAccessLayer();

            try
            {
                Profiles profile = profileAccess.GetProfileByEmail(login.Email);

                if (login.Email == profile.Email && Hash.Validate(login.Password, profile.Salt, profile.Password))
                {
                    user = profile;
                }
                user.Password = "";
                user.Salt = "";
                return user;
            }
            catch (Exception)
            {
                return user;
            }
        }

        public string GenerateToken(int size = 32)
        {
            var randomNumber = new byte[size];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        public class RefreshTokenModel
        {
            public string Jwt { get; set; }
            public string RefreshToken { get; set; }
        }

        public class LoginModel
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }

        private class UserModel
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime BirthDate { get; set; }
        }

        public struct CustomClaimTypes
        {
            public static string Permissions = "permission";
        }

    }
}
