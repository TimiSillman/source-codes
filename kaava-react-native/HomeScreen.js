import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  Animated,
  useNativeDriver,
  panResponder,
  ImageBackground,
} from "react-native";

//still in development, will change lot of these to components
//library imports
import { Icon, Button, Container, Header, Content, Right, Title } from 'native-base'
import {LongPressGestureHandler, PinchGestureHandler, State, PanGestureHandler} from 'react-native-gesture-handler'
import RadialMenu from 'react-native-radial-menu'
//custom components imports
import CustomHeader from './Components/CustomHeader'

class HomeScreen extends Component {
  constructor () {
    super()

    this.state = {
      lockArray: [],
      ActionButtonVisible: false,
      EventLocationXY: {x: 1, y: 1},
      EventLocationEndXY: {x: 0, y: 0},
      MenuPositionsDegree: [],
      MenuPositionRadian: [],
      MenuPositionsXY: [],
      MenuMinDist: 20,
      AnimItems: []
    }
    this.test.bind(this);
    this._baseScale = new Animated.Value(1);
    this._pinchScale = new Animated.Value(1);
    this._scale = Animated.multiply(this._baseScale, this._pinchScale);
    this._lastScale = 1;
    this._onPinchGestureEvent = Animated.event(
      [{ nativeEvent: { scale: this._pinchScale } }],
      { useNativeDriver: true }
    );
    this.springAnim = new Animated.ValueXY()


    _onPinchHandlerStateChange = event => {
      if (event.nativeEvent.oldState === State.ACTIVE) {
        this._lastScale *= event.nativeEvent.scale;
        this._baseScale.setValue(this._lastScale);
        this._pinchScale.setValue(1);
      }
    };

    this._translateX = new Animated.Value(0);
    this._translateY = new Animated.Value(0);
    this._lastOffset = { x: 0, y: 0 };
    this._onGestureEvent = Animated.event(
      [
        {
          nativeEvent: {
            translationX: this._translateX,
            translationY: this._translateY,
          },
        },
      ],
      { useNativeDriver: true }
    );
  }

  componentWillMount() {
        this.generateMenuPositions()
  }
  componentDidMount() {
  }

  generateMenuPositions = () => {
    let degreeIncrement = 360 / 8
    let a = 0
    var arrDeg = []
    var arrPosXY = []
    var arrRadian = []
    var arrAnimatedValue = []
    var objectDis = 100
    for (i = 0; i < 8; i++) {
      arrDeg.push(a)
      var radian = arrDeg[i]*Math.PI/180
      var posX = objectDis*Math.cos(radian)
      posX = parseInt(posX.toFixed())
      var posY = objectDis*Math.sin(radian)
      posY = parseInt(posY.toFixed())
      var AnimatedValue = new Animated.ValueXY({x:0, y:0})
      arrRadian.push(radian)
      arrPosXY.push({x:posX, y:posY})
      arrAnimatedValue.push({
        Anim: AnimatedValue,
        Pos: {x: posX, y: posY}
      })

      a = a + degreeIncrement
    }
    this.setState({
      MenuPositionsDegree: [...this.state.MenuPositionsDegree, ...arrDeg],
      MenuPositionRadian: [...this.state.MenuPositionRadian, ...arrRadian],
      MenuPositionsXY: [...this.state.MenuPositionsXY, ...arrPosXY],
      AnimItems: [...this.state.AnimItems, ...arrAnimatedValue]

     })
    console.log("generated")
  }
  _onHandlerStateChange = event => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      this._lastOffset.x += event.nativeEvent.translationX;
      this._lastOffset.y += event.nativeEvent.translationY;
      this._translateX.setOffset(this._lastOffset.x);
      this._translateX.setValue(0);
      this._translateY.setOffset(this._lastOffset.y);
      this._translateY.setValue(0);
    }
  };
  static navigationOptions = ({ navigation }) => ({
    title: "Home",
    headerRight: <Icon name="ios-menu" style={{  }} onPress={() => navigation.openDrawer()} />,
    drawerLabel: 'HERE COMES ALL FLOORPLANS',

  })

test = () => {
  console.log("long touch")
  let newAddedLock = {
    x:this.state.EventLocationXY.x,
   y:this.state.EventLocationXY.y
 }
  this.setState({
    lockArray: [...this.state.lockArray, newAddedLock]
  });

}

ActioButtonHandle = ({nativeEvent}) => {
  console.log(nativeEvent.state)
  if (this.state.ActionButtonVisible) {
    this.state.AnimItems.forEach((item, i) => {
      Animated.spring(item.Anim,{
        toValue: {x:0, y:0},
        friction: 10,
        useNativeDriver: true
      }).start()
      })
      setTimeout(() => {
    this.setState({
       ActionButtonVisible: false,
       lockArray: []
     });
   },2500);

   this.setState({
     EventLocationEndXY:{ x: nativeEvent.x - this.state.EventLocationXY.x, y: nativeEvent.y -  this.state.EventLocationXY.y}
   });

     setTimeout(() => {
     console.log("longpress out")
   },200);
   this.state.MenuPositionsXY.forEach((item, i ) => {
     var dis = Math.sqrt(Math.pow(item.x - (this.state.EventLocationXY.x - nativeEvent.x),2) + Math.pow(item.y - (this.state.EventLocationXY.y - nativeEvent.y),2))
     if(dis < 45) {
       console.log("position: " + item + " index: " + i)
     }
     console.log(dis)
   })
   } else {
     var arrLock = []
     this.state.AnimItems.forEach((item, i) => {
       item.Anim.Value = {x:nativeEvent.x, y:nativeEvent.y}
       let newAddedLock = {
         x:nativeEvent.x,
        y:nativeEvent.y,
        Anim: item.Anim
      }

       arrLock.push(newAddedLock)

       Animated.spring(item.Anim,{
         toValue: item.Pos,
         friction: 5,
         useNativeDriver: true
       }).start()

     })

     this.setState({
       lockArray: [...this.state.lockArray, ...arrLock],
       ActionButtonVisible: true,
       EventLocationXY: {x:nativeEvent.x, y:nativeEvent.y}
     });


     setTimeout(() => {
     console.log("longpress in")
   },200);
  }
}



  render() {
    let addedLocks = this.state.lockArray.map((lockArray, index) => {

    return (
        <Animated.Image key={index} source={require('./lock.png')} style={{
          top: lockArray.y -10 ,
          left: lockArray.x -10 ,
          position: 'absolute',
          width: 20,
          height: 20,
          alignItems: 'center',
          borderColor: 'red',
          borderWidth: 30,
          borderRadius: 1,
          transform: [
            {translateX: lockArray.Anim.x},
            {translateY: lockArray.Anim.y},
            {perspective: 1000}
          ],
        }} />

    )
  });

    return (

      <Container>
        <CustomHeader title="Kaava" drawerOpen={() => this.props.navigation.openDrawer()} />
        <Animated.View

       >
        <PanGestureHandler
        onGestureEvent={this._onGestureEvent}
        onHandlerStateChange={this._onHandlerStateChange}
        id="dragbox"
        shouldCancelWhenOutside
        >
        <Animated.View >
        <PinchGestureHandler
                id="image_pinch"
                onGestureEvent={this._onPinchGestureEvent}
                onHandlerStateChange={this._onPinchHandlerStateChange}
                shouldCancelWhenOutside
                simultaneousHandlers="dragbox"
                >
        <Animated.View style={{
        alignItems: 'center',}}>
        <LongPressGestureHandler
        onHandlerStateChange={ev => this.ActioButtonHandle(ev)}
        simultaneousHandlers="image_pinch"
        shouldCancelWhenOutside
        minDurationMs={800}
        maxDist={20}
        >
        <Animated.View style={{transform: [
         { perspective: 200 },
         { scale: this._scale },
         { translateX: this._translateX },
         { translateY: this._translateY },

       ],

     }}>

                  <Image
                    source={require('./floorplan-example.jpg')}
                    resizeMode='contain'
                  />

                  {addedLocks}

                  </Animated.View>

                  </LongPressGestureHandler>
              </Animated.View>
              </PinchGestureHandler>
              </Animated.View>
              </PanGestureHandler>
              </Animated.View>
      </Container>
    )
  }
}

export default HomeScreen;


const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'black',
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pinchableImage: {
    transform: [
      { perspective: 200 },
      { scale: this._scale },
    ],
  },
  wrapper: {
    flex: 1,
  },
  ActionButton: {

  },
});
