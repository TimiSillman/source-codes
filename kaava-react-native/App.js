import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  
} from "react-native";

//library imports 
import { Container, Content, Icon, Header, Body,Title } from 'native-base'
import { createDrawerNavigator, DrawerItems, SafeAreaView } from 'react-navigation'
import { Font , AppLoading} from 'expo';

//custom files 

import HomeScreen from "./HomeScreen";


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
    }
    
    async componentWillMount() {
    await Font.loadAsync({
    Roboto: require("native-base/Fonts/Roboto.ttf"),
    Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
    }
    render() {
    if (this.state.loading) {
    return(
      <Text>LOADING</Text>
    );
    }
    return (
      <MyApp />
    )
  }
}

const CustomDrawerContentComponent = (props) => (

  <Container>
    <Header style={styles.drawerHeader}>
      <Body>
        <Title style={styles.drawerImage}>KAAVA</Title>
      </Body>
    </Header>
    <Content>
      <DrawerItems {...props} />
    </Content>

  </Container>

);

const MyApp = createDrawerNavigator({

  
  Home: {
    screen: HomeScreen,
  },
  

},
  {
    initialRouteName: 'Home',
    drawerPosition: 'right',
    contentComponent: CustomDrawerContentComponent,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle'
  });


const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  drawerHeader: {
    height: 50,
    backgroundColor: 'white'
  },
  drawerImage: {
    top: 15,
    height: 50,
    width: 150,
    borderRadius: 25
  }

})
